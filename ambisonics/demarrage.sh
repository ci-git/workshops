#! /bin/sh
# lundi 15  février 2010
# script pour lancer le système audio de la performance 74 minutes
# copyright Olivier Heinry (olivier _AT_ heinry _DOT_ fr)
: << COMMENTBLOCK
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
COMMENTBLOCK

# on lance le serveur jackd via le programme qjackctl qui tue lui-même les instances des applications que l'on s'apprête à lancer
echo $PWD
pdextended -stderr -jack -r 48000 -channels 8 -alsamidi serveur.pd ~/pd-externals/mtl/1.mtlBrowser.pd &

#TODO: tester les répertoires avant d'écrire/copier
# questionner si c'est pd ou pdextended si les 2 sont installés.
